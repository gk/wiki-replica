<!-- include a simple overview of the service here -->

[[_TOC_]]

<!-- note: this template was designed based on multiple sources: -->
<!-- https://www.divio.com/blog/documentation/ -->
<!-- http://opsreportcard.com/section/9-->
<!-- http://opsreportcard.com/section/11 -->
<!-- comments like this one should be removed on instanciation -->

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Pager playbook

<!-- information about common errors from the monitoring system and -->
<!-- how to deal with them. this should be easy to follow: think of -->
<!-- your future self, in a stressful situation, tired and hungry. -->

## Disaster recovery

<!-- what to do if all goes to hell. e.g. restore from backups? -->
<!-- rebuild from scratch? not necessarily those procedures (e.g. see -->
<!-- "Installation" below but some pointers. -->

# Reference

## Installation

<!-- how to setup the service from scratch -->

## SLA

<!-- this describes an acceptable level of service for this service -->

## Design

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution", it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

<!-- a good guide to "audit" an existing project's design: -->
<!-- https://bluesock.org/~willkg/blog/dev/auditing_projects.html -->

<!-- things to evaluate here:

 * services
 * storage (databases? plain text files? cloud/S3 storage?)
 * queues (e.g. email queues, job queues, schedulers)
 * interfaces (e.g. webserver, commandline)
 * authentication (e.g. SSH, LDAP?)
 * programming languages, frameworks, versions
 * dependent services (e.g. authenticates against LDAP, or requires
   git pushes) 
 * deployments: how is code for this deployed (see also Installation)

how is this thing built, basically? -->

## Issues

<!-- such projects are never over. add a pointer to well-known issues -->
<!-- and show how to report problems. usually a link to the bugtracker -->

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search].

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues

## Maintainer, users, and upstream

<!-- document who deployed and operates this service, who the users -->
<!-- are, who the upstreams are, if they are still active, -->
<!-- collaborative, how do we keep up to date, -->

## Monitoring and testing

<!-- describe how this service is monitored and how it can be tested -->
<!-- after major changes like IP address changes or upgrades. describe -->
<!-- CI, test suites, linting, how security issues and upgrades are -->
<!-- tracked -->

## Logs and metrics

<!-- where are the logs? how long are they kept? any PII? -->
<!-- what about performance metrics? same questions -->

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

<!-- references to upstream documentation, if relevant -->

# Discussion

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. to quote the "audit procedure":

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Goals

<!-- include bugs to be fixed -->

### Must have

### Nice to have

### Non-Goals

## Approvals required

<!-- for example, legal, "vegas", accounting, current maintainer -->

## Proposed Solution

## Cost

## Alternatives considered

<!-- include benchmarks and procedure if relevant -->
