Lektor Debian Package lives on the Debian gitlab repository: https://salsa.debian.org/debian/lektor

The package is maintained and updated when a new Lektor version comes up.

Lektor needs a small dependency that is also packaged for Debian and lives at: https://salsa.debian.org/debian/python-inifile/

This is a small python library to handle INI files.
