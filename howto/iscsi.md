# Reference

## Installation

First, install the iSCSI support tools. This requires loading new
kernel modules, so we might need to reboot first to clear the module
loading protection:

    reboot
    apt install open-iscsi
