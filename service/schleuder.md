# Schleuder

## Using Schleuder

Schleuder has it's own gpg key, and also it's own keyring that you can use if you are subscribed to the list.

All command-emails need to be signed.

### Sending emails to people outside of the list

When using X-RESEND you need to add also the line X-LIST-NAME line to your email, and send it signed:

    X-LIST-NAME: listname@withtheemail.org
    X-RESEND: person@nogpgkey.org


You could also add their key to your schleuder mailing list, with

    X-LIST-NAME: listname@withtheemail.org
    X-ADD-KEY:
    [--- PGP armored block--]


And then do:

    X-LIST-NAME: listname@withtheemail.org
    X-RESEND-ENCRYPTED-ONLY: person@nogpgkey.org

### Getting the keys on a Schleuder list keyring

    X-LIST-NAME: listname@withtheemail.org
    X-LIST-KEYS 

And then:

    X-LIST-NAME: listname@withtheemail.org
    X-GET-KEY: someone@important.org

## Administration

Schleuder is a gpg-enabled mailing list manager with resending-capabilities. Subscribers can communicate encrypted (and pseudonymously) among themselves, receive emails from non-subscribers and send emails to non-subscribers via the list.

For more details see https://schleuder.nadir.org/docs/.

Schleuder runs on [Eugeni](https://db.torproject.org/machines.cgi?host=eugeni). The version of Schleuder currently installed is: 3.1.2

Mailing lists are managed through schleuder-cli which needs schleuder-api-daemon running. If the daemon is not running you can start it with:

    SCHLEUDER_CONFIG="/srv/schleuder.torproject.org/data/schleuder.yml" schleuder-api-daemon 

Lists also need to be setup in postfix in order to forward to Schleuder.

To create a list you can:

    schleuder-cli lists new secret-team@lists.torproject.org admin@torproject.org /path/to/public.key

Schleuder will create the list gpg key together with the list. Please not that the created keys do not expires. For more information about how Schlueder creates keys you can check: https://0xacab.org/schleuder/schleuder/blob/master/lib/schleuder/list_builder.rb#L120

To export a list public key you can do the following: 

    schleuder-cli keys export secret-team@lists.torproject.org <list-key-fingerprint>

Subscription are managed with the subscriptions command. To subscribe a new user to a list do:

    schleuder-cli subscriptions new secret-team@lists.torproject.org person@torproject.org <fingerprint> /path/to/public.key

All the other commands are available by typing:

    schleuder-cli help

